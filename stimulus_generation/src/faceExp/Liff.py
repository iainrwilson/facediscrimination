#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 10:41:53 2017

@author: iain

Meaning of Lif parser and selector


"""
import random

class Liff:
    """
    Encapsulates all of the place names and descriptions used
    
    """
    
    def __init__(self, filename):
        
        self.data = [line.rstrip() for line in open(filename)]
        
        self.liff = {}
        
        isTitle=True
        isText=False
        title=None
        last=None
        for l in self.data[4:]:
            if l == '':
                if isTitle:
                    #was title, not moving into text
                    isTitle=False
                    isText=True
                    last = l
                    continue
                if isText:
                    #was text, now next tile
                    isText=False
                    isTitle=True
                    last = l
                    continue
                if last == title:
                    isText = True
                    last = l
                    continue
            else:
                if isTitle:
                    title=l
                    self.liff[title] = ""
                    isTitle = False
                    #print title
                    last = l
                    continue
                elif isText:
                    #got text, append
                    if self.liff.has_key(title):
                        self.liff[title]+=" " +l
                    else:
                        self.liff[title] = l
                    last = l
                else:
                    if last == title:
                        #then must be text
                        isText = True
                        if self.liff.has_key(title):
                            self.liff[title] +=" " + l
                        else:
                            self.liff[title] = l
                        last = l 
    def getRandom(self):
        """ 
            Get a random Liff
        """
        titles = self.liff.keys()
        index = random.randint(0,len(titles)-1)
        title = titles[index]
        
        return title.title(), self.addReturn(self.liff[title])
    
    def getRandomFav(self):
        if self.index >= len(self.favs):
            self.index =0
        title = self.favs[self.index]
        self.index+=1
        return title.title(), self.addReturn(self.liff[title])
    
    def addReturn(self,text, count = 70):
        #remove double spaces
        out=''
        import re
        text = re.sub(' +',' ',text.strip(' '))
        i=0
        for c in text:
            if i > count:
                if c == ' ':
                    c ='\n'
                    i=0
            out+=c
            i+=1
        return out
    
    def loadFavorites(self,filename):
        """ 
        List of favorites
        """
        self.favs = [line.rstrip() for line in open(filename)]
        random.shuffle(self.favs)
        self.index=0
    
    def __getitem__(self, key):
        return self.liff[key]

if __name__ == "__main__":
   import os
   
   home = os.path.expanduser("~")
   dir = os.path.join(home,"experiments/faces/data")
   filename = os.path.join(dir,"meaning-of-liff.txt")
    
   liff  =   Liff(filename)
   liff.loadFavorites(os.path.join(dir,"meaning-of-liff-favs.txt"))
   
  # print liff.liff