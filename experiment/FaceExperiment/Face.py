import numpy as np
import math
import os

from scipy.interpolate import splprep, splev


class Face:
    """
    Class that encapsulates an individual face instance.
    """

    def __init__(self, img_filename=None,
                 tem_filename=None,
                 emb_filename=None,
                 loadimg=True,
                 loadtem=True,
                 create_blank=False):
        """
        Load in face from file:
            image filename
            feature coordinates filename
        """
        self.img = None# 
        if create_blank:
            self.img = np.zeros((1200,1200,3),np.uint8)
        self.points=None
        self.img_filename=img_filename
        self.tem_filename=tem_filename
        if img_filename is not None and loadimg:
            self.loadImage(img_filename)
        if tem_filename is not None and loadtem:
            self.loadTemplate(tem_filename)

        if emb_filename is not None:
            if os.path.exists(emb_filename):
                self.embeddings = np.load(emb_filename)

        else:
            tname = img_filename
            if img_filename is None:
                tname = tem_filename

            i = int(os.path.basename(tname)[6:-4])
            fname = "%s_%02d_emb.npy"%(os.path.basename(tname)[:5],i)

            self.tname = os.path.join(os.path.dirname(tname),fname)

            if os.path.exists(self.tname):
                self.embeddings = np.load(self.tname)
            # print("EMBL ",name)

            # print("Loading",emb_filename)
        self.hasFaceMask=False

    @classmethod
    def create(cls,image, points):
        face = cls()
        face.img = image
        face.points = points
        return face


    def loadTemplate(self, filename=None):
        """
        Load in data points from psycomorph data file.
        """

        if filename is not None:
            self.tem_filename = filename
        
        if not os.path.isfile(self.tem_filename):
            print("[LoadTemplate] File does not exist: ", self.tem_filename)
            return

        f = open(self.tem_filename, 'r')
        data = f.readlines()

        dataPoints = data[1:180]

        count = len(dataPoints)
        self.points = np.zeros([count, 2], np.int32)

        # clean data points, remove \n
        i = 0
        for d in dataPoints:
            p = d.strip("\n").split(' ')
            self.points[i, 0] = float(p[0])
            self.points[i, 1] = float(p[1])
            i = i + 1

        # self.ellipse = cv2.fitEllipse(self.points)
        # self.rect = cv2.minAreaRect(self.points)

    def distanceToFace(self, refFace, method="points"):
        """
        Calculate the euclidean distance to the reference face.
        """

        if method == "embeddings":
            if hasattr(self,"embeddings") and hasattr(refFace,"embeddings"):
                dist = np.sqrt(np.sum(np.square(np.subtract(self.embeddings, refFace.embeddings))))
                return dist
            else:
                return None

        if method == "points":
            points = self.noEarPoints() - refFace.noEarPoints()
            sum = 0
            for p in points:
                sum += math.sqrt(p[0] * p[0] + p[1] * p[1])

            return sum


    def distanceToInnerFace(self, refFace):
        """
        Calculate euclidean distance to the inner of the reference face
        """
        points = self.innerPoints() - refFace.innerPoints()
        sum = 0
        for p in points:
            sum += math.sqrt(p[0] * p[0] + p[1] * p[1])

        return sum

    def distanceToOuterFace(self, refFace):
        """ 
            
        """
        points = self.outerPoints() - refFace.outerPoints()
        sum = 0
        for p in points:
            sum += math.sqrt(p[0] * p[0] + p[1] * p[1])
        return sum

    def dotToPoints(self, refFace):
        """
        calculate the sum of the dot products 
        """
        s_points = self.noEarPoints()
        d_points = refFace.noEarPoints()
        cosine=[]
        cosine_sum=0
        for i in range(len(s_points)):
            s = s_points[i]/np.linalg.norm(s_points[i])
            d = d_points[i]/np.linalg.norm(d_points[i])
            try:
                cosine_sum+=math.degrees(math.acos(np.dot(s,d)))
            except ValueError:
                #print "ValueError: %f" % (np.dot(s,d)), s,d
                pass
            
        return cosine_sum  
            
    
    def innerPoints(self):
        """
        return list of points for the inner face
        """
        return np.concatenate((self.points[0:108], self.points[158:]), axis=0)

    def outerPoints(self,points=None):
        """
        return a list of points for the outer face / no ear
        """
        #return self.points[108:158]
        if points is None:
            return np.concatenate((self.points[108:115], self.points[125:158]),axis=0)
        return np.concatenate((points[108:115], points[125:158]), axis=0)

    def noEarPoints(self):
        """
        return a list of points without the ears
        """
        return np.concatenate((self.points[:115], self.points[125:]), axis=0)

    def faceAndHair(self):
        """
        return list of points with hair, face but no ears ... used for masking
        """
        return np.concatenate(
            (self.points[110:112], self.points[125:133], self.points[113:115][::-1], self.points[145:158][::-1]),
            axis=0)

    def facePoints(self):
        """
        Return list of points that enclose just the face, ignoring hair.
        """
        return np.concatenate(
                (self.points[109:112],self.points[125:134],self.points[112:115][::-1],self.points[134:145][::-1]),
                axis=0)


    def eyes(self):
        """
        return a tuple of the centre points of the eyes
        """
        return (self.points[0], self.points[1])

    def faceCentre(self):
        """
        return midpoint between eyes - there maybe a better centre point for face?
        """

        leye = self.points[0]
        reye = self.points[1]
        return leye + ((reye - leye) / 2)

    def eyeVec(self):
        left, right = self.eyes()
        vec = np.asarray([(right[0] - left[0]), (right[1] - left[1])])
        # normalise and return
        return (vec / np.linalg.norm(vec))

    def saveTemplate(self, filename):
        f = open(self.tem_filename, 'r')
        data = f.readlines()

        for i in range(len(self.points)):
            data[i + 1] = "%.5f %.5f\n" % (self.points[i][0], self.points[i][1])

        out_file = open(filename, 'w')
        out_file.writelines(data)
        out_file.close()
