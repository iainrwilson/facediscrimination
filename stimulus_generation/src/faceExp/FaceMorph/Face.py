import numpy as np
import math
import os
import cv2
from scipy.interpolate import splprep, splev


class Face:
    """
    Class that encapsulates an individual face instance.
    """

    def __init__(self, img_filename=None, tem_filename=None, loadimg=True,loadtem=True,create_blank=False):
        """
        Load in face from file:
            image filename
            feature coordinates filename
        """
        self.img = None# 
        if create_blank:
            self.img = np.zeros((1200,1200,3),np.uint8)
        self.points=None
        self.img_filename=img_filename
        self.tem_filename=tem_filename
        if img_filename is not None and loadimg:
            self.loadImage(img_filename)
        if tem_filename is not None and loadtem:
            self.loadTemplate(tem_filename)

        self.hasFaceMask=False

    @classmethod
    def create(cls,image, points):
        face = cls()
        face.img = image
        face.points = points
        return face

    def loadImage(self, filename=None):
        if filename is not None:
            self.img_filename = filename
            
        if (os.path.isfile(self.img_filename)):
            self.img = cv2.imread(self.img_filename)
            # print "Loading :: ",filename
        else:
            print("[LoadImage] File does not exist: ", filename)


    def getImage(self,gray=False):
        if self.img is None:
            self.loadImage()
        if not gray:
            return self.img
        return cv2.cvtColor(self.img,cv2.COLOR_BGR2GRAY)

    def getEnhancedImage(self,suffix='cartoon'):
        return cv2.imread(self.img_filename+'.%s.png'%suffix)
        

    def loadTemplate(self, filename=None):
        """
        Load in data points from psycomorph data file.
        """

        if filename is not None:
            self.tem_filename = filename
        
        if not os.path.isfile(self.tem_filename):
            print("[LoadTemplate] File does not exist: ", self.tem_filename)
            return

        f = open(self.tem_filename, 'r', encoding="utf8", errors='ignore')
        data = f.readlines()

        dataPoints = data[1:180]

        count = len(dataPoints)
        self.points = np.zeros([count, 2], np.int32)

        # clean data points, remove \n
        i = 0
        for d in dataPoints:
            p = d.strip("\n").split(' ')
            self.points[i, 0] = float(p[0])
            self.points[i, 1] = float(p[1])
            i = i + 1

        self.ellipse = cv2.fitEllipse(self.points)
        self.rect = cv2.minAreaRect(self.points)

    def distanceToFace(self, refFace):
        """
        Calculate the euclidean distance to the reference face.
        """

        points = self.noEarPoints() - refFace.noEarPoints()
        sum = 0
        for p in points:
            sum += math.sqrt(p[0] * p[0] + p[1] * p[1])

        return sum

    def distanceToInnerFace(self, refFace):
        """
        Calculate euclidean distance to the inner of the reference face
        """
        points = self.innerPoints() - refFace.innerPoints()
        sum = 0
        for p in points:
            sum += math.sqrt(p[0] * p[0] + p[1] * p[1])

        return sum

    def distanceToOuterFace(self, refFace):
        """ 
            
        """
        points = self.outerPoints() - refFace.outerPoints()
        sum = 0
        for p in points:
            sum += math.sqrt(p[0] * p[0] + p[1] * p[1])
        return sum

    def dotToPoints(self, refFace):
        """
        calculate the sum of the dot products 
        """
        s_points = self.noEarPoints()
        d_points = refFace.noEarPoints()
        cosine=[]
        cosine_sum=0
        for i in range(len(s_points)):
            s = s_points[i]/np.linalg.norm(s_points[i])
            d = d_points[i]/np.linalg.norm(d_points[i])
            try:
                cosine_sum+=math.degrees(math.acos(np.dot(s,d)))
            except ValueError:
                #print "ValueError: %f" % (np.dot(s,d)), s,d
                pass
            
        return cosine_sum  
            
    
    def innerPoints(self):
        """
        return list of points for the inner face
        """
        return np.concatenate((self.points[0:108], self.points[158:]), axis=0)

    def outerPoints(self,points=None):
        """
        return a list of points for the outer face / no ear
        """
        #return self.points[108:158]
        if points is None:
            return np.concatenate((self.points[108:115], self.points[125:158]),axis=0)
        return np.concatenate((points[108:115], points[125:158]), axis=0)

    def noEarPoints(self):
        """
        return a list of points without the ears
        """
        return np.concatenate((self.points[:115], self.points[125:]), axis=0)

    def faceAndHair(self):
        """
        return list of points with hair, face but no ears ... used for masking
        """
        return np.concatenate(
            (self.points[110:112], self.points[125:133], self.points[113:115][::-1], self.points[145:158][::-1]),
            axis=0)

    def facePoints(self):
        """
        Return list of points that encompase just the face, ignoring hair.
        """
        return np.concatenate(
                (self.points[109:112],self.points[125:134],self.points[112:115][::-1],self.points[134:145][::-1]),
                axis=0)


    def eyes(self):
        """
        return a tuple of the centre points of the eyes
        """
        return (self.points[0], self.points[1])

    def faceCentre(self):
        """
        return midpoint between eyes - there maybe a better centre point for face
        """


        leye = self.points[0]
        reye = self.points[1]
        return leye + ((reye - leye) / 2)

    def eyeVec(self):
        left, right = self.eyes()
        vec = np.asarray([(right[0] - left[0]), (right[1] - left[1])])
        # normalise and return
        return (vec / np.linalg.norm(vec))

    def boundingRect(self):
        """
        Return the bounding rectangle for all the points
        """
        return cv2.boundingRect(self.points)

    def scaleTo(self, refFace):
        """
        Scale this face to match the given referecnce
        """

        # check the two face are the same size
        self.resize(refFace.img.shape)

        height = self.boundingRect()[3]
        dest_height = refFace.boundingRect()[3]

        scale = float(dest_height) / float(height)

        print("Scaling by:: ", scale)

        dst = cv2.resize(self.img, None, fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)

        y = int((dst.shape[0] - self.img.shape[0]) / 2)
        h = y + self.img.shape[0]
        x = int((dst.shape[1] - self.img.shape[1]) / 2)
        w = x + self.img.shape[1]
        if (scale > 1.0):
            self.img = dst[y:h, x:w]

        self.points =  self.points.astype(float) * scale
        self.points -= [x, y]

        # //need to re-aling - transpose only,

    def resize(self, size):
        """
        Resize image and points
        """

        scale = float(size[0]) / float(self.img.shape[0])
        if (scale == 1):
            return

        print("size miss match... %f" % scale)
        dst = cv2.resize(self.img, None, fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)
        self.img = dst
        self.points *= scale

    def alignCentre(self, refFace):
        diff = self.faceCentre() - refFace.faceCentre()
        self.points += diff

    def alignTo(self, refFace):
        """
        Align this face to the given reference
        """
        print("\nAligning face to: ")

        # check the two face are the same size
        self.resize(refFace.img.shape)

        print(refFace.eyeVec(), self.eyeVec())

        ref_eyeVec = refFace.eyeVec()
        ref_eyeVec[0] = round(ref_eyeVec[0], 5)
        ref_eyeVec[1] = round(ref_eyeVec[1], 5)

        eyeVec = self.eyeVec()
        eyeVec[0] = round(eyeVec[0], 5)
        eyeVec[1] = round(eyeVec[1], 5)

        # calculate the angle between the two eye lines.
        if ((ref_eyeVec != eyeVec)[0]):
            d =  np.dot(ref_eyeVec, eyeVec)
            if d>1.0:
                theta=0
            else:
                theta = math.acos(np.dot(ref_eyeVec, eyeVec))
            R = math.degrees(theta)
        else:
            R = 0

        # what is the direction of rotation.
        baseline = np.asarray([0, 1])
        if np.dot(refFace.eyeVec(), baseline) > np.dot(self.eyeVec(), baseline):
            R *= -1

        print("\tEye angluar difference ::", R)

        # calculate translation
        T = refFace.faceCentre() - self.faceCentre()
        print("\tTranslation ", T)

        rows, cols, depth = self.img.shape

        if (abs(R) > 1):
            # rotate image

            M = cv2.getRotationMatrix2D((cols / 2, rows / 2), R, 1)
            self.img = cv2.warpAffine(self.img, M, (cols, rows))
            # rotate points
            for i in range(len(self.points)):
                self.points[i][0] = M[0][0] * self.points[i][0] + M[0][1] * self.points[i][1] + M[0][2]
                self.points[i][1] = M[1][0] * self.points[i][0] + M[1][1] * self.points[i][1] + M[1][2]

        # translate image
        M = np.float32([[1, 0, T[0]], [0, 1, T[1]]])
        self.img = cv2.warpAffine(self.img, M, (cols, rows))

        # translate points
        for i in range(len(self.points)):
            self.points[i][0] += T[0]
            self.points[i][1] += T[1]
        return T, R

    def drawPoints(self, colour, img=None):
        if img is None:
            img = self.img
        for p in self.points:
            cv2.circle(img, (p[0], p[1]), 2, colour, -1)

    def drawLines(self, colour, refFace):
        """
        Draw a line from one this to the reference face point.
        """
        for i in range(len(self.points)):
            self.drawArrow(self.img, (self.points[i][0], self.points[i][1]),
                           (refFace.points[i][0], refFace.points[i][1]), 5)
            # cv2.line(self.img,,colour,2)

    def drawArrow(self, img, pt1, pt2, tipLength):

        colour = (0, 0, 255)
        size = 2
        tipSize = abs(pt1 - pt2) * tipLength
        angle = math.atan2(pt1[1] - pt2[1], pt1[0] - pt2[0])

        cv2.line(img, pt1, pt2, colour, size)

        pt3 = (pt2[0] + tipSize * math.cos(angle + math.pi / 4)), (pt2[1] + tipSize * math.sin(angle + math.pi / 4))
        cv2.line(img, pt3, pt3, colour, size)

    def drawEllipse(self):
        cv2.ellipse(self.img, self.ellipse, (0, 255, 0), 2)
        print(self.ellipse)
        print(self.rect)
        box = cv2.cv.BoxPoints(self.rect)
        box = np.int0(box)
        cv2.drawContours(self.img, [box], 0, (0, 0, 255), 2)

    def save(self, filename=None):
        if (filename == None):
            filename = self.img_filename + "_new.jpg"
        else:
            head, tail = os.path.split(self.img_filename)
            filename = os.path.join(filename, tail)

        print("saving to: ", filename)
        self.saveImage(filename)

    def saveImage(self, filename):
        cv2.imwrite(filename, self.img)

    def saveMaskedImage(self, filename):
        cv2.imwrite(filename, self.alpha)

    def saveMask(self, filename):
        cv2.imwrite(filename, self.mask)

    def saveTemplate(self, filename):
        f = open(self.tem_filename, 'r', errors='ignore')
        data = f.readlines()

        for i in range(len(self.points)):
            data[i + 1] = "%.5f %.5f\n" % (self.points[i][0], self.points[i][1])

        out_file = open(filename, 'w')
        out_file.writelines(data)
        out_file.close()

    def applyMask(self, smooth,points=None):
        if points is None:
            points = self.faceAndHair()
        self.mask = np.zeros((self.img.shape[0], self.img.shape[1], 1), np.uint8)
        cv2.fillPoly(self.mask, [self.createSmoothMask(self.faceAndHair()).astype(int)], 255)

        #kernel = np.ones((5, 5), np.uint8)
        #self.mask = cv2.erode(self.mask, kernel, iterations=4)

        for i in range(smooth):
            self.mask = cv2.GaussianBlur(self.mask, (15, 15), 15)
        temp = cv2.cvtColor(self.mask, cv2.COLOR_GRAY2BGR)
        temp = temp.astype(float) / 255.
        self.masked_img = (self.img.astype(float) * temp)

        # use mask as an alpha channel
        alpha = cv2.cvtColor(self.masked_img.astype(np.uint8), cv2.COLOR_BGR2BGRA)
        b, g, r, a = cv2.split(alpha)
        self.alpha = cv2.merge((b, g, r, self.mask))

    def createSmoothMask(self, points):
        tck, u = splprep(points.T, u=None, s=0.0, per=1)
        u_new = np.linspace(u.min(), u.max(), 1000)
        x_new, y_new = splev(u_new, tck, der=0)

        return np.column_stack((x_new, y_new))

    def createFaceMask(self):
        """
        Create mask of just the face, not hair
        """
        if self.hasFaceMask:
            return
        #interpolate the feature points 
        self.faceMask = np.zeros((self.img.shape[0], self.img.shape[1], 1), np.uint8)
        cv2.fillPoly(self.faceMask, [self.createSmoothMask(self.facePoints()).astype(int)], 255)
        self.hasFaceMask=True
    
    def faceMaskDifference(self,dst):
        """
        Return the number of pixels differet between the two facemasks / trying out another distance metric
        """
        
        #make sure we have created face masks
        self.createFaceMask()
        dst.createFaceMask()
        
        diff = cv2.bitwise_xor(self.faceMask,dst.faceMask)
        return cv2.countNonZero(diff)
        


    def createDelaunay(self,points=None, img=None):
        if img is None:
            img = self.img
        if points is None:
            points = self.points
        rect = (0, 0, img.shape[1], img.shape[0])
        subdiv = cv2.Subdiv2D(rect)
        for p in points:
            subdiv.insert((int(p[0]), int(p[1])))

        self.triangles = subdiv.getTriangleList()

    def drawDelaunay(self, img=None):
        if img is None:
            img = self.img
        def rect_contains(rect, point):
            if point[0] < rect[0]:
                return False
            elif point[1] < rect[1]:
                return False
            elif point[0] > rect[2]:
                return False
            elif point[1] > rect[3]:
                return False
            return True

        triangleList = self.triangles
        size = img.shape
        r = (0, 0, size[1], size[0])
        delaunay_color = (255, 255, 255)

        for t in triangleList:

            pt1 = (t[0], t[1])
            pt2 = (t[2], t[3])
            pt3 = (t[4], t[5])

            if rect_contains(r, pt1) and rect_contains(r, pt2) and rect_contains(r, pt3):
                cv2.line(img, pt1, pt2, delaunay_color, 1, cv2.LINE_AA, 0)
                cv2.line(img, pt2, pt3, delaunay_color, 1, cv2.LINE_AA, 0)
                cv2.line(img, pt3, pt1, delaunay_color, 1, cv2.LINE_AA, 0)

    def averageLuminance(self):
        # convert to YUV
        yuv = cv2.cvtColor(self.img, cv2.COLOR_BGR2YUV)
        y, u, v = cv2.split(yuv)
        return cv2.mean(y)
