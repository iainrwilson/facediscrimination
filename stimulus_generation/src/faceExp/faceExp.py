import os, math, random

import Quest
from os.path import expanduser


class QuestStairs:
    """
	Class that manages multiple QUEST staircase objects
	tGuess , prior threshold guess
	tGuessSD, standard deviation assigned to the guess
	pThreshold, threshold criteria as a probability of response==1
	beta, steepness of the Weibull slope
	delta, fraction of trials on which the observer presses blindly
	gamma, fraction of trials that will generate a response 1, when intensity=-inf
	"""

    quest_objects = {}
    maxVal = 7186
    tGuess = math.log10(0.2 * maxVal)  # 3.157517209
    tGuessSD = 2.0  # 0.15
    pThreshold = 0.625
    delta = 0.075
    gamma = 0.25
    beta = 3.5
    grain = 0.01
    # def __init__(self):
    # self.add("normal")
    # self.add("cartoon")

    def clear(self):
        """ clear all quest objects """
        self.quest_objects.clear()

    def add(self, name):
        """ add a new quest object to the list, does nothing if already exisits """
        if not name in self.quest_objects:
            self.quest_objects[name] = Quest.QuestObject(self.tGuess, self.tGuessSD, self.pThreshold, self.beta,
                                                         self.delta, self.gamma, grain=self.grain)

    def reset(self):
        """ reset all existing QUEST objects """
        keys = quest_objects.keys()
        del self.quest_objects
        self.quest_objects = {}
        for k in keys:
            self.addQuest(k)

    def quantile(self, name):
        """ get the next stimulus level recommendation, quest.quantile() """
        return self.quest_objects[name].quantile()

    def mean(self, name):
        """
		get the mean of the QUEST posterior pdf 
		Get the mean threshold estimate
		
		"""
        return self.quest_objects[name].mean()

    def mode(self, name):
        """
		Mode of the QUEST posterior pdf
		't' is the mode thershold estimate
		'p' is the value of the unnormalised pdf at t
		"""
        return self.quest_objects[name].mode()

    def sd(self, name):
        """ Standard deviation of QUEST porsterior pdf """
        return self.quest_objects[name].sd()

    def update(self, name, intensity, response):
        """
		update the QUEST posteriro pdf
		Update self to reflest the resulst of the trial.
		"""
        self.quest_objects[name].update(intensity, response)


class FaceExp:
    limits = {}
    faces = {}
    previous = {}
    index = 0

    def __init__(self, limitFilename, distancesFilename):
        self.limitFilename = limitFilename
        self.distancesFilename = distancesFilename
        self.loadFaceLimitData(self.limitFilename)
        self.loadDistanceData(self.distancesFilename)
        self.prepareList()
        self.maxDist = self.getMaximumDistance()

    def loadFaceLimitData(self, filename):
        """
		Load limit data for faces, e.g. which faces to use, and where to start/finish. 
		
		Format: <5 char, facename> <min face number> <max Face numer> <extra info>
		
		e.g. F1002,1,23,None
		"""

        if (not os.path.exists(filename)):
            raise Exception("Face data file: %s not found!" % filename)

        f = open(filename, 'r')
        data = f.readlines()
        # data = d[0].split('\r'data[1])

        faces = {}
        for i in range(len(data) - 1):
            tmp = data[i + 1].split(',')
            faces[tmp[0]] = tmp[1:]

        self.limits = faces

    def loadDistanceData(self, filename):
        """
		Load distance data for all faces from the average.
		"""

        if (not os.path.exists(filename)):
            raise Exception("Face data file: %s not found!" % filename)

        f = open(filename, 'r')
        data = f.readlines()

        store = {}
        for i in range(len(data)):
            data[i] = data[i].strip('\n').split(',')

            if data[i][0][-6:-5].isdigit():
                title = data[i][0][:-6]
            else:
                title = data[i][0][:-5]

            if not (title in store):
                store[title] = {}

            store[title][int(data[i][0][len(title):-4])] = (float(data[i][1]), float(data[i][2]), float(data[i][3]))

        self.faces = store

    def prepareList(self):
        """
		remove bad faces and randomise the order of the list of faces.

		"""
        for k in self.faces.keys():
            # check is valid
            if (self.limits[k[:5]][0] == 'x'):
                # invalid...delte
                del self.faces[k]
        # shuffle keys
        self.faces_keys = self.faces.keys()
        random.shuffle(self.faces_keys)

    def shuffleList(self):
        """ shuffle, so it can be called inbetween trials"""
        random.shuffle(self.faces_keys)

    def getFace(self, name, intensity):
        """
            Get a specific face e.g. F1021 at a set distance.
            """

        distance = math.pow(10, intensity)

        # check name is in list
        f_name = None
        for k in self.faces.keys():
            if k[:5] == name:
                f_name = k

        if f_name == None:
            print "face not found"
            return None

        face = self.faces[f_name]
        limit = self.limits[f_name[:5]]
        # face is assumed to be valid.
        if limit[0] == 'x':
            print "Bad Face %s" % f_name
            return None

        maxDist = face[int(limit[0])][0] - face[int(limit[1])][0]
        reqDist = distance
        minStep = face[39][0] #step distance between faces.
        lowerLimit = int(limit[0])
        upperLimit = int(limit[1])
        numSteps = upperLimit - lowerLimit
        center = lowerLimit+math.floor(numSteps / 2)
        print "Limits: ", lowerLimit, upperLimit
        
        #check if requested distance is larger that avaliabl range
        if (abs(maxDist) < reqDist):
            print "Max Distance Exceeded"
            reqDist = maxDist
            #now set the output to be the ranges.
            low = lowerLimit
            high = upperLimit
            steps = high-low
        else:   
            #calculate the number of steps required
            steps = round(float(reqDist) / float(minStep))    
            if steps == 0:
                steps = 1
            
           
            low = center - math.floor(steps/2)
            high = center + math.floor(steps/2)         
            
            if (steps % 2 != 0):
                #then odd
                if not (low -1) < lowerLimit:
                    low -= 1
                elif not (high +1) > upperLimit:
                    high += 1


        actualDist = face[low][0] - face[high][0]
        print "[GetFace()]" ,low, high, steps, center, reqDist
        return f_name, low, high, math.log10(actualDist)

    def getNextFace(self, intensity):
        """
		Get the next face in the list, with a set distance. If distance cannot be achieved, just use the largest avaliable.
		name is 5 char face identifier e.g. F1001
                increment internal pointer
		"""

        if self.index >= len(self.faces):
            return None

        f_name = self.faces_keys[self.index]
        self.index += 1
        return self.getFace(f_name[:5], intensity)

    def getMaximumDistance(self):
        maximum = 0
        count = 0
        ssum = 0
        for k, v in self.faces.iteritems():
            limit = self.limits[k[:5]]
            if not (limit[0] == 'x'):
                dist = abs(v[int(limit[0])][0] - v[int(limit[1])][0])
                ssum += dist
                count += 1
                if (maximum < dist):
                    maximum = dist
        # print "Avergage: %f" % ( float(ssum)/float(count) )
        return maximum


class MainExp:
    faces = {}
    indexs = {}
    skip_count = 7  # """ Every n trials insert high intensity stimulus to keep participant happy """
    skip_value = 3.45
    index = 0

    def __init__(self, baseDir, imgDir):
        self.baseDir = baseDir
        self.imgDir = imgDir
        self.distanceFilename = os.path.join(baseDir, "distances.csv")
        self.faceDataFilename = os.path.join(baseDir, "face-data.csv")
        self.quests = QuestStairs()

    def setDistancesFile(self, filename):
        self.distanceFilename = os.path.join(self.baseDir,filename)

    def setFaceDataFile(self, filename):
        self.faceDataFilename = os.path.join(self.baseDir, filename)

    
    def add(self, name,distances=None, faceData=None):
        """
			Add a type to the experiemtns list. e.g normal, cartoon.
			created face data set and quest objects
		"""
        dfile = self.distanceFilename
        ffile = self.faceDataFilename
        if distances is not None:
            dfile = os.path.join(self.baseDir,distances)
        if faceData is not None:
            ffile = os.path.join(self.baseDir,faceData)
            
        if not name in self.faces:   
            self.faces[name] = FaceExp(ffile, dfile)
            self.quests.add(name)
            self.indexs[name] = 0

    def getFilename(self, name, fname, index):
        if (name == "cartoon"):
            return "%s%d.jpg.cartoon.png" % (fname, index)
        elif (name == "normal"):
            return "%s%d.png" % (fname, index)
        if (name == "practice_cartoon"):
            return "%s%d.jpg.cartoon.png" % (fname, index)
        elif (name == "practice_normal"):
            return "%s%d.png" % (fname, index)
        else:
            return "%s%d.png" % (fname, index)

    def getNextFace(self, name, intensity=None):
        """
		Get the next face in the sequence, returns low face, high face full filenames and actual intensity
		"""
        if (intensity == None):
            intensity = self.quests.quantile(name)

        if self.index >= self.skip_count:
            intensity = self.skip_value
            self.index = 0

        f_name, lowIndex, highIndex, intensity = self.faces[name].getNextFace(intensity)
        lowFace = os.path.join(self.imgDir, self.getFilename(name, f_name, lowIndex))
        highFace = os.path.join(self.imgDir, self.getFilename(name, f_name, highIndex))

        self.index += 1

        return lowFace, highFace, intensity

    def getFace(self, name, intensity=None):
        """
		Get a specific face, used for the face calibration exp.
                Returns low face, high face full filenames and actual intensity
		"""
        if (intensity == None):
            intensity = self.quests.quantile(name)

        if self.index >= self.skip_count:
            intensity = self.skip_value
            self.index = 0

        f_name, lowIndex, highIndex, intensity = self.faces[name].getFace(name, intensity)
        lowFace = os.path.join(self.imgDir, self.getFilename(name, f_name, lowIndex))
        highFace = os.path.join(self.imgDir, self.getFilename(name, f_name, highIndex))

        #increment index
        self.index +=1
        return lowFace, highFace, intensity

    def update(self, name, intensity, response):
        self.quests.update(name, intensity, response)


if __name__ == "__main__":

    visual_angle = 4
    distance_to_screen = 1000
    dot_pitch = 0.233
	# var.scotoma_radius=math.tan(math.radians(visual_angle))*distance_to_screen/dot_pitch/2


    # define local data store
    home = expanduser("~")

    baseDir = os.path.join(home, "experiments/faces/data")
    imgDir = os.path.join(baseDir, "morph/masked")

    experiment = MainExp(baseDir, imgDir)
    experiment.add("normal")
    experiment.add("cartoon")
    experiment.add("practice_normal")
    experiment.add("practice_cartoon")

    # find the variance in the inner to outer maximum distances

    faces = experiment.faces['normal'].faces
    limits = experiment.faces['normal'].limits

    out = {}
    for k, v in faces.iteritems():
        limit = limits[k[:5]]
        total = v[int(limit[0])][0] - v[int(limit[1])][0]
        inner = v[int(limit[0])][1] - v[int(limit[1])][1]
        outer = v[int(limit[0])][2] - v[int(limit[1])][2]
		# print k[:5], total, inner- outer
        out[k[:5]] = (inner - outer)

    import operator

    sorted_out = sorted(out.items(), key=operator.itemgetter(1))

    for v in sorted_out:
        print v[0], ",", v[1]
