# -*- coding: utf-8 -*-

"""
New version of the Face morphing experiment 

Experiment class


"""

import FaceMorph
import Quest
import os
import math
import psychopy.data as pquest


class Staircase:
    """
        Wrapper class for staircases;
        Currently implements Quest... 
    """
     

    """
    Quest parameters
    Used for percentage morph (1->40) 

    minVal = math.log10(1)
    maxVal = math.log10(40)
    srange = math.log10(40)
    tGuess = math.log10(20)
    tGuessSD = math.log10(20)
    pThreshold = 0.625
    delta = 0.075
    gamma = 0.25
    beta = 3.5
    grain = 0.01

    New Quest values for euclid distance stuff 

    minVal = 2.0 (100)
    maxVal = 4.0 (10000)
    tGuess = 3.0
    tGuessSD = 2.0
    srange = 1.5 (double check that) 
    

    """
    


    QUEST = "quest"
    PQUEST = "pquest"
    

    def __init__(self,staircase=QUEST,nTrials=None,method="scale"):
        self.staircases = None
        self.type = staircase
        self.nTrials = nTrials
        self.index = 0
        
        if method=="euclid":
            self.minVal = 2.0
            self.maxVal = 4.0
            self.tGuess = 3.0
            self.tGuessSD = 1.5
            self.srange = 1.5
        else:
             self.minVal = math.log10(1)
             self.maxVal = math.log10(40)
             self.srange = math.log10(40)
             self.tGuess = math.log10(20)
             self.tGuessSD = math.log10(20)
        
        self.pThreshold = 0.625
        self.delta = 0.075
        self.gamma = 0.25
        self.beta = 3.5
        self.grain = 0.01

        self.add()
        
    def add(self):
        """ 
        add a new staircase object to the list 
        
        """
        
        
        if self.type == self.QUEST:
            self.staircase = Quest.QuestObject(self.tGuess,\
                                              self.tGuessSD, self.pThreshold,\
                                              self.beta,self.delta, self.gamma,\
                                              grain=self.grain)
        elif self.type == self.PQUEST:
            self.staircase = pquest.QuestHandler(
                       self.tGuess,
                       self.tGuessSD,
                       pThreshold=self.pThreshold,
                       beta=self.beta,
                       gamma=self.gamma,
                       delta=self.delta,
                       grain=self.grain,
                       minVal=self.minVal,
                       nTrials=self.nTrials,
                       maxVal=self.maxVal,
                       range=self.srange)
    def next(self):
        """
            Advances to next trial number: Gets next stimulus value
        """        
        self.index +=1
        if self.type == self.QUEST:
            return self.staircase.quantile(),  self.index-1
        
        elif self.type == self.PQUEST:
            return self.staircase.next(), self.index-1
        
    def update(self, value, response):        
        r = 0
        if response is True:
            r=1
        
        if self.type == self.QUEST:
            self.staircase.update(value,r)
        
        elif self.type == self.PQUEST:
            self.staircase.addResponse(r,intensity=value)


    def mean(self):
        if self.type == self.QUEST:
            return self.staircase.mean()
        
        elif self.type == self.PQUEST:
            return self.staircase.mean()

    def quantile(self):
        if self.type == self.QUEST:
            return self.staircase.quantile()
        
        elif self.type == self.PQUEST:
            return self.staircase.quantile()
    

class Experiment:
    
    """
        Experiemnt class, 
        init - loads face data from source folders.
        add - adds a new experiment (as before)
        
    """
   
    
    #important filenames
    #limit_filename = "new_face-data.csv"
    limit_filename = "face-data-average.csv"

    nTrials = 50
    
    
    def __init__(self, datadir=None,face_folder="masked"):
        """ 
        datadir is the main faces exp data folder.
        data_folder contains all the faces.
        limit = the file containig all the limits.
        """
        self.face_folder=face_folder
        self.datadir = datadir
        if self.datadir is None:
            home = os.path.expanduser("~")
            self.datadir = "experiments/faces/data"
            self.datadir = os.path.join(home,self.datadir)
        
        self.data_folder = os.path.join(self.datadir,self.face_folder)
        self.limit_file = os.path.join(self.datadir,self.limit_filename)
        self.faces = {} 
        self.staircases = {}
        
        #hack to speed up dev...
        #self.addSingle("M1008")
        #self.addMulti("normal")
        
    def __getitem__(self, key):
        return self.faces[key]
    
     
    
    def addSingle(self, name, staircase=Staircase.PQUEST,method="euclid"):
        """
            add a new Staricase based on a single identity,
            creates a new staricase object 
            load face from folder
        """
        if (name in self.staircases) or (name in self.faces) :
            raise Exception("[Experiment::addSingle()] Staricase %s already added!" % name)
        
        
        #check face is has limits
        
        self.staircases[name] = Staircase(staircase=staircase,method=method)
        
        self.faces[name] = FaceMorph.FaceSet(datadir=self.data_folder,\
                                      limitfile=self.limit_file,\
                                      face=name)
        
    def addMulti(self, name, limit_file=None, staircase=Staircase.PQUEST,method="euclid",orderfile=None):
        """
            Add a new Staricase with mulitple identites aka "normal" 
            
            creates new staricase object, with fixed number of trials
            
            creates list of data files, from limit_file 

        UPDATE: 19/06/17 - add another filename for stimulus order list - then order array accordingly.


        """
        
        if (name in self.staircases) or (name in self.faces) :
            raise Exception("[Experiment::addMulti()] Staricase %s already added!" % name)
            
        self.staircases[name] = Staircase(staircase=staircase, nTrials=self.nTrials,method=method)
       
        
        if limit_file is None:
            limit_file = self.limit_file    
        else:
            limit_file = os.path.join(self.datadir,limit_file)
            
        print "Limit File: " , limit_file
            
        self.faces[name] = FaceMorph.FaceSet(datadir=self.data_folder, limitfile=limit_file,orderfile=orderfile)
        

    def nextFace(self, name, method="euclid",distance=None):
        """
            get the next stimulus for the specific experiment,
        
            useses percentage morph - 1 step = 5%
            
            return basename, low filename , high filename , requested_intensity, actual_intensity
        """
        
        if name not in self.faces:
            raise Exception("Staircase %s not found" % name)
        
        intensity, index = self.staircases[name].next()
        
        #used for overriding intensity .e.g used in practice
        if distance is not None:
            intensity = distance
        
        
        #round to nearest float and then convet to integer.
        #intensity = int(round(intensity))
        
        #for a multiFace index is that in the list.
        if len(self.faces[name].face_keys) > 1:
            face_name = self.faces[name].face_keys[index]
        else:
            face_name = name
        
        if method == "scale":
            l,h = self.faces[name].getIndex(face_name, math.pow(10,intensity))
            actual_intensity = l-h
        elif method == "euclid":
            l,h,d = self.faces[name].getEuclidIndex(face_name, math.pow(10,intensity))
            actual_intensity = d
                
            
            
        print math.pow(10,intensity)
        requested_intensity = intensity
        
        l_fname = self.faces[name].getFilename(name=face_name,index=l)
        h_fname = self.faces[name].getFilename(name=face_name,index=h)
        
        return { "face":face_name,
                "low_filename":l_fname,
                "high_filename":h_fname,
                "requested_intensity":requested_intensity,
                "actual_intensity":math.log10(actual_intensity),
                "raw_intensity":actual_intensity,
                "index":index}
        
    def update(self, name, value, response):
        """
        update the staircase with a repsonse,
        Value is the presented stimuli distance
        response is correct or not
        """
        if name not in self.staircases:
            raise Exception("Staircase %s not found" % name)
            
        self.staircases[name].update(value,response)
    
    def mean(self,name):
        """
        Get the current mean of the threshold estimate
        """
        if name not in self.staircases:
            raise Exception("Staircase %s not found" % name)
            
        return self.staircases[name].mean()
    
    def quantile(self,name):
        """
        Get the current quantile of the threshold estimate
        """
        if name not in self.staircases:
            raise Exception("Staircase %s not found" % name)
            
        return self.staircases[name].quantile()
                  
        
if __name__ == "__main__":
    
    """ test the experiment """
    
    home = os.path.expanduser("~")
    datadir = "experiments/faces/data"
    
    alt_limit = "new-face-data.csv"
    
    exp = Experiment(os.path.join(home,datadir))
    
    exp.addSingle("M1008")
    
    exp.addMulti("normal")
    
    
    
