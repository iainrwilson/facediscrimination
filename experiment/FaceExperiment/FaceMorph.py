import os, glob
from .Face import Face
import random
import numpy as np

class FaceSet:

    """
    A Class that encapsulates an instance of one identity and all its morphed parts.
    i.e. one stimulus set used in the faces experiments.

    To start with it will just containing existing face images and feature coordinates, also pulling information
    about the constraints - some of the extreme morphing causes aberrations so the min and max are limited.

    Adding:
        Visualisation tools, for viewing features.

    Later this can expand to:
        generating new morphed faces.
        PCA analysis of the faces set
        And other cool things.

    """

    EUCLID = "euclid"
    DOT = "dot"
    EMBEDDINGS = "embeddings"

    def __init__(self, datadir=None,limitfile=None, face=None, orderfile=None):
        """
        Load in the config files:
        List of Face base names - can / should be auto-generated from a folder.
        Limits config - defines the max/min for this particular face set.
        Data Dir - where the data is actualy stored.

        """
        self.faces={}
        self.limitfile = None
        self.dataDir = None
        self.limits = None
        if limitfile is not None:
            self.loadLimitData(limitfile)
        
        if datadir is not None:
            # if limitfile is None:
            #     self._loadFromDataDir(datadir=datadir, face=face)
            # else:
            self.loadFromDataDir(datadir=datadir, face=face)
            
        if orderfile is not None:
            """ re-order the self.face_keys to match the order file"""
            self.reorder(orderfile)
    
    def reorder(self, orderfile):
        """ re-order the self.face_keys to match the order file"""
        self.face_keys = [line.rstrip() for line in open(os.path.join(self.datadir+"/../",orderfile))]
        

    def setDataDir(self, datadir):
        if (not os.path.exists(datadir)):
            raise Exception("Dir: %s not found!" % datadir)
        
        self.datadir = datadir

    def setLimitFilename(self,filename):
        if (not os.path.exists(filename)):
            raise Exception("Limit File: %s not found!" % filename)
        
        self.limitfile = filename
    
    def __getitem__(self,key):
        return self.faces[key]
    
    def iteritems(self):
        return self.faces.iteritems()

    def loadLimitData(self,filename=None):
        """
        Load limit data for faces, e.g. which faces to use, and where to start/finish. 
		
        mat: <5 char, facename> <min face number> <max Face numer> <extra info>
		
	    e.g F1002,1,23,None
        """
        if filename is not None:
            self.setLimitFilename(filename)

        f = open(self.limitfile, 'r')
        data = f.readlines()
        
        faces = {}
        for i in range(len(data) - 1):
            tmp = data[i + 1].split(',')
            faces[tmp[0]] = tmp[1:]

        self.limits = faces   

    def loadFromDataDir(self,datadir=None,face=None):
        """
                Parse DataDir and generate set of faces, 5 charachter name is the key
                if face is set,

                Ignores limit file


            //also reads the embeddings file.
        """


        if datadir is not None:
            self.setDataDir(datadir)

        search_string = self.datadir + "/[F|M]*[0-9].png"

        if face is not None:
            search_string = self.datadir + "/%s*[0-9].png" % face

        face_files = glob.glob(search_string)

        # print("Files found: %d" % len(face_files))

        if len(face_files) == 0:
            raise Exception("[FaceSet::loadFromDataDir()] no faces found in %s" % search_string)

        for face in face_files:
            fname = os.path.basename(face)
            if hasattr(self,'limits') and self.limits is not None:
                if fname[:5] not in self.limits:
                    # print "not using:%s"%face
                    continue

                if self.limits[fname[:5]][0] == 'x':
                    continue

            # if fname[:5] == name:
            # separate out morph number:
            title = fname[:5]
            if fname[-6:-5].isdigit():
                m_n = int(fname[-6:-4])
            else:
                m_n = int(fname[-5:-4])

            if not (title in self.faces):
                self.faces[title] = {}

            emb = os.path.join(os.path.dirname(face),"%s_%02d_emb.npy"%(title,m_n))
            self.faces[title][m_n] = Face(img_filename=face,
                                          tem_filename=face[:-4] + ".tem",
                                          emb_filename=emb,
                                          loadimg=False,
                                          loadtem=True)

        # create a keylist of faces
        self.face_keys = self.faces.keys()

        # shuffle the keys....
        # random.shuffle(self.face_keys)

        if len(self.faces) == 0:
            raise Exception("[FaceSet::loadFromDataDir()] no valid faces found in: %s" % self.datadir)

    def _loadFromDataDir(self, datadir=None, face=None):
        """
        Parse DataDir and generate set of faces, 5 charachter name is the key
        if face is set, limts search to one face.
        face must exist in limits file
        """
        
        if self.limits is None:
            raise Exception("Need to load the limits file")
        
        
        if datadir is not None:
             self.setDataDir(datadir)
        
        search_string = self.datadir+"/[F|M]*[0-9].png"
        
        if face is not None:
            if face not in self.limits:
                raise Exception("[FaceSet::loadFromDataDir()] face not in limit file")
            search_string = self.datadir+"/%s*[0-9].png" % face
        
        #print "searching : ", search_string
        
        face_files = glob.glob(search_string)
        
        print("Files found: %d" % len(face_files))
        
        if len(face_files) == 0:
            raise Exception("[FaceSet::loadFromDataDir()] no faces found in %s" % search_string)

        for face in face_files:
            fname = os.path.basename(face)
            if fname[:5] not in self.limits:
                #print "not using:%s"%face
                continue
            
            if self.limits[fname[:5]][0] == 'x':
                continue
            
            #if fname[:5] == name:
                #separate out morph number:
            title = fname[:5]
            if fname[-6:-5].isdigit():
                m_n = int(fname[-6:-4])
            else:
                m_n = int(fname[-5:-4])
                
            if not (title in self.faces):
                self.faces[title]={}

            self.faces[title][m_n] = Face(img_filename=face, tem_filename=face[:-4] + ".tem", loadimg=False, loadtem=False)
        
        #create a keylist of faces
        self.face_keys = list(self.faces.keys())
        
        #shuffle the keys....
        random.shuffle(self.face_keys)
        
        if len(self.faces) == 0:
            raise Exception("[FaceSet::loadFromDataDir()] no valid faces found in: %s" % self.datadir)
        
    def hasFace(self,name):
        """
        Check the face exists
        """
        
        return (name in self.faces) and (name in self.limits)
    
    def getOriginalFace(self, name):
        """
        return un-morphed face : 20
        """

        if name not in self.faces:
            return None

        return self.faces[name][20]

    def getFilename(self, name=None, index=20):
        """
            get the full path and filename for teh face image
        """
        if name is None:
            #assumes only one face
            return self.faces[self.face_keys[0]][index].img_filename
        
        return self.faces[name][index].img_filename

    def getFaceObject(self,name,index=20):
        return self.faces[name][index]
        
    def getFilenames(self, name=None, size=40):
        """
            get filenames for face, within enforcing range limits
        """
        
        if name is None:
            name = self.face_keys[0]
        
        l,h = self.getIndex(name, size)
        
        return self.getFilename(name=name,index=l), self.getFilename(name=name,index=h)
        
    # def getIndex(self, name, size):
    #     """
    #         get the low and high indexs for a given face, 
    #         enforcing range limits, 
    #         center on the middle of avalibable range
    #     """
    #     
    #     #round to nearest float and then convet to integer.
    #     size = int(round(size))
    #     
    #     l = int(self.limits[name][0])
    #     h = int(self.limits[name][1])
    #     
    #     #print name, l, h 
    #     
    #     m = l+((h-l)/2) #don't worry about odd/even 
    #     
    #     if size % 2 == 1:
    #         minVal = m-(size+1)/2
    #         maxVal = m+(size+1)/2
    #         maxVal-=1
    #     else:
    #         minVal = m-size/2
    #         maxVal = m+size/2
    #         
    #     if minVal >= l:
    #         l=minVal
    #     if maxVal <= h:
    #         h=maxVal
    #     
    #     return l,h
    
    def getIndex(self,name,distance,method=EUCLID):
        """
        get the low and the high indexes for a given face, 
        using euclidean distance
        center on middle
        """
        
        l = int(self.limits[name][0])
        h = int(self.limits[name][1])
        
        m = l+((h-l)/2) #don't worry about odd/even 

        max_distance = self.getDistance(self.faces[name][l],self.faces[name][h],calctype=method)
        min_distance = self.getDistance(self.faces[name][21],self.faces[name][22],calctype=method)

        #print("%s Max: %f Min: %f"%(name,max_distance,min_distance))

        arr = []
        #calculate average
        for i in range(40):
            arr.append(self.getDistance(self.faces[name][i],self.faces[name][i+1],calctype=method))
            
        avg = np.average(arr)
        std = np.std(arr)
        
        
        """  Check if requested distance is in range, if not return max,min or just a single step."""
        if max_distance < distance:
            # print("Max Distance hit")
            return l,h,max_distance

        if min_distance > distance:
            return 21,22,min_distance
        
        """ roughly calculate how many steps are required, round to nearest """

        steps = int(round(distance / avg))

        # print( "Min: %f Max %f Avg: %f Std: %f steps:%d" %(min_distance, max_distance,avg,std,steps))
        
        """ split along the middle """

        if steps % 2 == 1:
            minVal = m-(steps+1)/2
            maxVal = m+(steps+1)/2
            maxVal -= 1
        else:
            minVal = m-steps/2
            maxVal = m+steps/2
        
        """ ensure maxVal does not go out of range """
        if maxVal > h:
            maxVal = h
        if minVal < l:
            minVal = l



        def check(min,max,up=True):
            """ check real distance """

            #also check we are not over stepping the limits.

            if(min<l):
                min=l

            if(max>h):
                max=h


            # print("Checking Distance: min:%f max%f"%(min,max))
            real_distance = self.getDistance(self.faces[name][min], self.faces[name][max], calctype=method)

            #we have no more room, so return.
            if (min <= l and max >= h):
                return min, max, real_distance

            diff = (((distance - real_distance)) / avg)
            """ check for how close the distance was """
            # print("Requested: %f, Found: %f L:%d H:%d Diff: %f (round %f)" % (distance, real_distance, min, max, diff,round(diff)))
            diff = round(diff)
            if diff < 1.0:
                # """ Return the closest of the two guesses"""
                # if abs(distance - new_d) < abs(distance - real_distance):
                #     return minVal - lowinc, maxVal + highinc, new_d
                # else:
                #     return minVal, maxVal, real_distance
                return min,max,real_distance
            """ try steping down or up, depending on how far away the guess was """

            lowinc = 0
            highinc =0
            #if only one, go high
            if diff == 1.0:
                if up:
                    highinc=1
                else:
                    lowinc=1
            else:
                if diff % 2 == 1:
                    lowinc = (diff+1)/2
                    highinc = (diff+1)/2
                    highinc-=1
                else:
                    lowinc = diff/2
                    highinc = diff/2

            # new_d = self.getDistance(self.faces[name][min-lowinc],self.faces[name][max+highinc],calctype=method)
            # print("Re-requested: %f, Found: %f L:%d H:%d " %(distance, new_d, min-lowinc,max+highinc))
            return check(min-lowinc,max+highinc,up=not(up))

        min,max,real = check(int(minVal),int(maxVal))
        # print("[FaceMorph::getIndex] min:%02f max:%02f real:%02f"%(min,max,real))
        return min,max,real


    def getDistance(self,src,dst,calctype=EUCLID):

        #check points are loaded
        if src.points is None:
            src.loadTemplate()
        if dst.points is None:
            dst.loadTemplate()
        
        if calctype==self.EUCLID:
            return src.distanceToFace(dst)
        if calctype == self.EMBEDDINGS:
            return src.distanceToFace(dst,method=self.EMBEDDINGS)
        elif calctype==self.DOT:
            return src.dotToPoints(dst)

    def getDifficulty(self,name,start,end,calctype=EUCLID):

        return self.getDistance(self.faces[name][start],self.faces[name][end],calctype=calctype)

    def getDifficultyRange(self,name,calctype=EUCLID):
        """
        get the range of difficulty, min max for a given face
        """

        if name not in self.faces:
            return None

        min_limit = int(self.limits[name][0])
        max_limit = int(self.limits[name][1])
        
        maxi = self.getDifficulty(name,min_limit,max_limit,calctype=calctype)
        mini = self.getDifficulty(name,min_limit,min_limit+1,calctype=calctype)

        return mini,maxi
