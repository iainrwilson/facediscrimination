""" Morph faces using java commandline """
import numpy as np
import sys
import os
import subprocess
from faceExp.FaceMorph import Face
"""
Moprh source face to destination in 5% increments
Destination Face is a specific identity
Source Face is the average face.

usage:
python morphFaces.py <source_face> <destination_face>


"""
home = os.path.expanduser("~")

#set your output folder.
dst_dir = os.path.join(home,"tmp")
cwd = os.path.dirname(os.path.realpath(__file__))

src_face = os.path.realpath(sys.argv[1])
dst_face = os.path.realpath(sys.argv[2])



#dst_face  = os.path.join(home,"experiments/faces/data/average/M1014.jpg")
#src_face  = os.path.join(home,"experiments/faces/data/male_average.jpg")


s_face = Face.Face(img_filename=src_face,tem_filename=src_face[:-4]+".tem")
d_face = Face.Face(img_filename=dst_face,tem_filename=dst_face[:-4]+".tem")


d_face.alignTo(s_face)
d_face.scaleTo(s_face)
d_face.alignTo(s_face)

#save to tmp
dst_img = os.path.join(dst_dir,os.path.basename(dst_face))
dst_tem = os.path.join(dst_dir,os.path.basename(dst_face[:-4]+".tem"))
d_face.saveImage(dst_img)
d_face.saveTemplate(dst_tem)

cmd = "java -classpath .:FaceMorphLib6.jar FacemorphTest "+os.path.realpath(src_face)+" "+os.path.realpath(dst_img)
wrd = os.path.join(cwd,"java")


min = -1.0
max = 1.0
step = 0.05

ranges = np.arange(min,max+step,step,dtype=np.float)

# ranges = [1.0]

index = 0
for r in ranges:
    out_face = "%s_%d" % (os.path.basename(dst_face)[:-4],index)

    exec_cmd="%s %.2f %s"%(cmd,r,os.path.join(dst_dir,out_face))
    print("running ", exec_cmd)
    subprocess.call(exec_cmd,cwd=wrd,shell=True)


    #apply mask
    f_out_face = os.path.join(dst_dir,out_face)
    face = Face.Face(img_filename=f_out_face+".jpg",tem_filename=f_out_face+".tem")
    face.applyMask(5)
    fname = f_out_face+".png"
    face.saveMaskedImage(fname)


    print(exec_cmd)
    index+=1

