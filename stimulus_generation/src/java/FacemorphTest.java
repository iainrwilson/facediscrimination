import Facemorph.ImageToJpeg;
import Facemorph.Mask;
import Facemorph.PCA;
import Facemorph.Template;
import Facemorph.Transformer;
import Facemorph.Filter;
import java.awt.Image;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Float;
import java.awt.image.ImageObserver;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Vector;
import javax.swing.ImageIcon;
import java.util.ArrayList;
import java.text.DecimalFormat;



public class FacemorphTest
{
  public FacemorphTest() {}
  
  public static void main(String[] args)
  {
     

    /*
      Test,read in only source, destination images and templates.

      src_img,  dst_tmp, ammount (percentage)

      subject is the same as source.
     */

      if ( !( (args.length==3) || (args.length==4)) ){
	System.out.println(String.format("Sort it out :: got %d expecting 3",args.length));
	return;
    }

    
    

    String src_fname = args[0];
    String dst_fname = args[1];
    String src_tname = src_fname.substring(0,src_fname.length()-4)+".tem";
    String dst_tname = dst_fname.substring(0,dst_fname.length()-4)+".tem";

    System.out.println("src: "+src_tname);
    
    ImageIcon ii = new ImageIcon(src_fname);
    ImageObserver ob = ii.getImageObserver();
    Image subject = ii.getImage();
    Image source = new ImageIcon(src_fname).getImage();
    Image target = new ImageIcon(dst_fname).getImage();
    //Image pcaimg = new ImageIcon(args[9]).getImage();


    

    Template subtem = new Template();
    Template srctem = new Template();
    Template tartem = new Template();
    Template trantem = new Template();
    //Template pcatem = new Template();
    if ((!subtem.read(src_tname)) || (!srctem.read(src_tname)) || (!tartem.read(dst_tname))) {
      System.out.println("Error reading templates, closing");
      return;
    }
    
    long starttime = System.currentTimeMillis();
    
    try
    {
	/*      Mask mask = new Mask();
      mask.read(new File(args[7]));
      

      PCA pca = new PCA();
      pca.readText(args[8]);
       */

      double shape = java.lang.Float.parseFloat(args[2]);
      double colour = 0.0D;
      double texture= 0.0D;

      boolean speed = false; 
      Mask mask = new Mask();
      //Filter[] filters;
     
      Point2D.Float p = subtem.getPoint(0);
      Point2D.Float leftEye = new Point2D.Float(p.x, p.y);
      p = subtem.getPoint(1);
      Point2D.Float rightEye = new Point2D.Float(p.x, p.y);
      p = subtem.getPoint(96);
      Point2D.Float mouth = new Point2D.Float(p.x, p.y);
      
      // subtem.normaliseEyes(leftEye, rightEye,0, 1);

      // 0 = multiscale, 1 linear, 2 tps;
      int warpType = 0;

      int normalisation= 3;
      int[] normpoints = {0,1,96};
      
      Image trans = Transformer.testTransform(warpType, subtem, srctem, tartem, trantem,
					       subject, source, target, shape, colour, texture,
					       null, 1, null, true, true, true,normalisation, normpoints );

      String out_fname ="output.jpg";

      if(args.length==3){
	  out_fname = src_fname.substring(0,src_fname.length()-4);
	  String value = new DecimalFormat("#.##").format(shape);
	  //formatter.setRoundingMode( RoundingMode.DOWN );
	  out_fname +="_"+ value;// java.lang.Double.toString(shape);
      }else{
	  out_fname = args[3];
      }

      System.out.println("output filename: "+out_fname);
      

      DataOutputStream dis = new DataOutputStream(new FileOutputStream(out_fname+".jpg"));
      ImageToJpeg.writeJpeg(trans,dis,ob);
      trantem.write(out_fname+".tem");

      
      /*
      for (int i = 0; i < sequence.size(); i++) {
        DataOutputStream dis = new DataOutputStream(new FileOutputStream("output_" + i + ".jpg"));
        subject = (Image)sequence.elementAt(i);
        ImageToJpeg.writeJpeg(subject, dis, ob);
	}*/
    }
    catch (Exception e) {
      System.out.println("Error: " + e);
    }
    long endtime = System.currentTimeMillis();
    float time = (float)(endtime - starttime) / 1000.0F;
    System.out.println("Time = " + time);
  }
}
